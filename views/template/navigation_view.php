
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo base_url(); ?>">Test MVC Application</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li <?php echo (isset($this->data['active_tab']) && $this->data['active_tab'] == 1) ? ' class="active"' : ''; ?>><a href="<?php echo base_url('products/all'); ?>">All Products</a></li>
				<li <?php echo (isset($this->data['active_tab']) && $this->data['active_tab'] == 2) ? ' class="active"' : ''; ?>><a href="<?php echo base_url('products/add'); ?>">Add Product</a></li>
			</ul>
		</div>
	</div>
</nav>
