<div class="container">
	<div class="jumbotron">
		<table class="table table-stripped table-hover">
			<tr>
				<th></th>
				<th>Product Name</th>
				<th>Product Number</th>
				<th>Product Description</th>
				<th>Stock</th>
				<th>Buy price</th>
				<th>Sell price</th>
				<th>VAT</th>
				<th></th>
			</tr>
		<?php foreach ($this->data['products']['result'] as $value): ?>
			<tr>
				<td style="width: 100px"><img class="img img-responsive" src="<?php echo $value['image_url']; ?>" alt="<?php echo $value['product_name']; ?>" ></td>
				<td><?php echo $value['product_name']; ?></td>
				<td><?php echo $value['product_number']; ?></td>
				<td style="width: 380px"><?php echo nl2br(character_limiter($value['product_desc'], 150)); ?></td>
				<td><?php echo $value['stock']; ?></td>
				<td><?php echo $value['buy_price']; ?></td>
				<td><?php echo $value['sell_price']; ?></td>
				<td><?php echo $value['vat_percent']; ?></td>
				<td style="width: 100px">
					<a href="<?php echo base_url('products/edit/' . $value['product_id']); ?>" class="btn btn-success"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
					<div class="btn btn-danger delete-product" data-id="<?php echo $value['product_id']; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></div>
				</td>
			</tr>
		<?php endforeach;?>
		</table>
	</div>
</div>