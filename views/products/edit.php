
<div class="container">
	<div class="jumbotron">
		<form class="form" id="edit-form">
			<div class="form-group">
				<label for="product_name">Product name</label>
				<input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product name" value="<?php echo $this->data['product']['product_name']; ?>">
			</div>
			<div class="form-group">
				<label for="product_number">Product number</label>
				<input type="text" class="form-control" id="product_number" name="product_number" placeholder="Product number" value="<?php echo $this->data['product']['product_number']; ?>">
			</div>

			<div class="form-group">
				<label for="product_desc">Product Description</label>
				<textarea type="text" class="form-control" id="product_desc" name="product_desc"><?php echo $this->data['product']['product_desc']; ?></textarea>
			</div>


			<div class="form-group">
				<label for="stock">Stock</label>
				<input type="text" class="form-control" id="stock" name="stock" placeholder="Buy Price" value="<?php echo $this->data['product']['stock']; ?>">
			</div>
			<div class="form-group">
				<label for="buy_price">Buy Price</label>
				<input type="text" class="form-control" id="buy_price" name="buy_price" placeholder="Buy Price" value="<?php echo $this->data['product']['buy_price']; ?>">
			</div>
			<div class="form-group">
				<label for="sell_price">Sell Price <small>is calculated to 30% from Buy Price</small></label>
				<input type="text" class="form-control" id="sell_price" name="sell_price" placeholder="Sell Price" value="<?php echo $this->data['product']['sell_price']; ?>">
			</div>
			<div class="form-group">
				<label for="vat_percent">VAT %</label>
				<input type="text" class="form-control" id="vat_percent" name="vat_percent" placeholder="Vat %" value="<?php echo $this->data['product']['vat_percent']; ?>">
			</div>

			<div class="form-group">
				<label for="image">Image</label>
				<input type="file" id="image" name="image">
				<input type="hidden" name="product_image" value="<?php echo $this->data['product']['product_image']; ?>">
				<span>Current Image: <img class="img img-responsive" src="<?php echo $this->data['product']['image_url']; ?>" alt="<?php echo $this->data['product']['product_name']; ?>" style="width: 100px"><small>pick other to change it</small></span>
			</div>
			<button type="submit" class="btn btn-default pull-right" id="edit" data-id="<?php echo $this->data['product']['product_id']; ?>">Submit</button>
		</form>
	</div>
</div>
