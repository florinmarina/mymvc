<div class="container">
	<div class="jumbotron">
		<form action="<?php base_url('products/add_product');?>" id="add-form">
			<div class="form-group">
				<label for="product_name">Product name</label>
				<input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product name">
			</div>
			<div class="form-group">
				<label for="product_number">Product number</label>
				<input type="text" class="form-control" id="product_number" name="product_number" placeholder="Product number">
			</div>

			<div class="form-group">
				<label for="product_desc">Product Description</label>
				<textarea type="text" class="form-control" id="product_desc" name="product_desc"></textarea>
			</div>


			<div class="form-group">
				<label for="stock">Stock</label>
				<input type="text" class="form-control" id="stock" name="stock" placeholder="Stock">
			</div>
			<div class="form-group">
				<label for="buy_price">Buy Price</label>
				<input type="text" class="form-control" id="buy_price" name="buy_price" placeholder="Buy Price">
			</div>
			<div class="form-group">
				<label for="sell_price">Sell Price <small>is calculated to 30% from Buy Price</small></label>
				<input type="text" class="form-control" id="sell_price" name="sell_price" placeholder="Sell Price">
			</div>
			<div class="form-group">
				<label for="vat_percent">VAT %</label>
				<input type="text" class="form-control" id="vat_percent" name="vat_percent" placeholder="Vat %">
			</div>

			<div class="form-group">
				<label for="image">Image</label>
				<input type="file" id="image" name="image">
			</div>
			<button type="submit" class="btn btn-default pull-right" id="save">Submit</button>
		</form>
	</div>
</div>