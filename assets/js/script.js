var myDelay = (function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function Redirect(url) {
    window.location.href = url;
}

function delayRedirect(url, time) {
    myDelay(function() {
        Redirect(url);
    }, time);
}
$(function() {
    $('#save').on('click', function(event) {
        event.preventDefault();
        var data = new FormData($('#add-form')[0]);
        $.ajax({
            url: base_url + 'products/add_product',
            type: 'POST',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(data) {
                if (typeof data.content.url !== 'undefined') {
                    alert(data.content.message);
                    delayRedirect(data.content.url, 500);
                }
            }
        });
    });
    var vat = 25;
    var adaos = 30;
    var calc = {
        sell_price: function(buy_price) {
            return parseFloat(buy_price) + (parseFloat(buy_price) * (adaos / 100));
        },
        vat_percent: function(buy_price) {
            return parseFloat(buy_price) - (parseFloat(buy_price) * ((100 - vat) / 100));
        }
    };
    $('input[name="buy_price"]').on('keyup', function(event) {
        event.preventDefault();
        $('input[name="vat_percent"]').val(calc.vat_percent($(this).val()))
        $('input[name="sell_price"]').val(calc.sell_price($(this).val()))
    });
    $('.delete-product').on('click', function(event) {
        event.preventDefault();
        if (confirm('Are you sure')) {
            $.ajax({
                url: base_url + 'products/delete_product/' + $(this).data('id'),
                type: 'post',
                dataType: 'json',
                success: function(data) {
                    if (typeof data.content.url !== 'undefined') {
                        delayRedirect(data.content.url, 200);
                    }
                }
            });
        }
    });
    $('#edit').on('click', function(event) {
        event.preventDefault();
        var _this = $(this);
        var data = new FormData($('#edit-form')[0]);
        $.ajax({
            url: base_url + 'products/edit_product/' + _this.data('id'),
            type: 'POST',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            success: function(data) {
                if (typeof data.content.url !== 'undefined') {
                    alert(data.content.message);
                    delayRedirect(data.content.url, 500);
                }
            }
        });
    });
});