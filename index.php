<?php

/**
 * @Author:    Florin Marina
 * @Date Created: 2018-05-02 03:12:28
 * @Last Modified by:    Florin Marina
 * @Last Modified time: 2018-05-02 15:26:41
 */

define('APP_PATH', dirname(__FILE__));

error_reporting(E_ALL);
ini_set('display_errors', 1);

require 'libs/Bootstrap.php';
require 'libs/Controller.php';
require 'libs/Model.php';
require 'libs/View.php';

// libraries
require 'libs/Json.php';
require 'libs/Database.php';

// config files
require 'config/config.php';
require 'config/database.php';

// helper functions
require 'helpers/app.php';

$app = new Bootstrap();
