<?php

/**
 * @Author:    Florin Marina
 * @Date Created: 2018-05-02 10:25:44
 * @Last Modified by:    Florin Marina
 * @Last Modified time: 2018-05-02 12:27:29
 */

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'testmvc');
