<?php

/**
 * @Author:    Florin Marina
 * @Date Created: 2018-05-02 09:26:15
 * @Last Modified by:    Florin Marina
 * @Last Modified time: 2018-05-02 18:09:40
 */
class Products_model extends Model
{
    protected $_table       = 'products';
    protected $_primary_key = 'product_id';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * get product by id
     * @param $id -- product_id
     */
    public function get_product($id)
    {
        $params                      = [];
        $params[$this->_primary_key] = $id;

        $product = $this->get_all($params);

        return $product['result'][0];
    }

    /**
     * get all products
     * @param
     */
    public function get_all($params = [])
    {

        $query_from = $this->_table;

        $query_join = null;

        $query_where = null;

        if (isset($params[$this->_primary_key])) {
            $query_where .= $this->_primary_key . " = " . $params[$this->_primary_key];
        }

        $this->db->select($query_from, '*', $query_join, $query_where);

        $get_all = $this->db->get_result();

        $result = [];
        foreach ($get_all as $value) {
            array_push($result, $this->format_products($value));
        }
        return [
            'result' => $result,
            'query'  => $this->db->get_sql(), // just for debug purpose
        ];
    }

    /**
     * format result to array
     * @param $row
     */
    public function format_products($row)
    {
        if (empty($row)) {
            return [];
        }

        return [
            'product_id'     => (int) $row['product_id'],
            'product_name'   => $row['product_name'],
            'product_number' => $row['product_number'],
            'product_desc'   => $row['product_desc'],
            'product_image'  => $row['product_image'],
            'image_url'      => (file_exists(UPLOAD_PATH . $row['product_image']) ? UPLOAD_URL . $row['product_image'] : base_url('assets/images/no_image_available.png')),
            'stock'          => (float) $row['stock'],
            'buy_price'      => (float) $row['buy_price'],
            'sell_price'     => (float) $row['sell_price'],
            'vat_percent'    => (int) $row['vat_percent'],

        ];
    }

    /**
     * save product
     * @param $data
     */
    public function save_product($data)
    {
        $insert = [
            'product_name'   => $this->db->escape_string($data['product_name']),
            'product_number' => $this->db->escape_string($data['product_number']),
            'product_desc'   => $this->db->escape_string($data['product_desc']),
            'stock'          => $this->db->escape_string($data['stock']),
            'buy_price'      => $this->db->escape_string($data['buy_price']),
            'sell_price'     => $this->db->escape_string($data['sell_price']),
            'vat_percent'    => $this->db->escape_string($data['vat_percent']),
            'product_image'  => $this->db->escape_string($data['product_image']),
        ];

        $this->db->insert($this->_table, $insert);
        if ($this->db->num_rows() > 0) {
            return true;
        }
        return false;
    }

    /**
     * save product
     * @param $data
     */
    public function update_product($id, $data)
    {
        $update = [
            'product_name'   => $this->db->escape_string($data['product_name']),
            'product_number' => $this->db->escape_string($data['product_number']),
            'product_desc'   => $this->db->escape_string($data['product_desc']),
            'stock'          => $this->db->escape_string($data['stock']),
            'buy_price'      => $this->db->escape_string($data['buy_price']),
            'sell_price'     => $this->db->escape_string($data['sell_price']),
            'vat_percent'    => $this->db->escape_string($data['vat_percent']),
            'product_image'  => $this->db->escape_string($data['product_image']),
        ];

        $where = $this->_primary_key . " = " . $id;

        $this->db->update($this->_table, $update, $where);

        if ($this->db->num_rows() > 0) {
            return true;
        }
        return false;
    }

    /**
     * save product
     * @param $data
     */
    public function delete_product($id)
    {
        $where = $this->_primary_key . " = " . $id;

        $this->db->delete($this->_table, $where);

        if ($this->db->num_rows() > 0) {
            return true;
        }
        return false;
    }
}
