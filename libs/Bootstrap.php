<?php

/**
 * @Author:    Florin Marina
 * @Date Created: 2018-05-02 05:00:05
 * @Last Modified by:    Florin Marina
 * @Last Modified time: 2018-05-02 10:57:08
 */
class Bootstrap
{
    public function __construct()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;

        $url = rtrim($url, '/');

        $url = explode('/', $url);

        if (empty($url[0])) {
            require 'controllers/index.php';
            $controller = new Index();

            if (method_exists($controller, 'index')) {
                $controller->index();
            }
            return false;
        }

        $file = 'controllers/' . $url[0] . '.php';

        if (file_exists($file)) {
            require $file;
        } else {
            require 'controllers/errors.php';
            $error = new Errors();
            $error->controller();
            return false;
        }

        $controller = new $url[0];

        if (isset($url[2])) {
            $controller->{$url[1]}($url[2]);
        } else {

            if (isset($url[1])) {
                $controller->{$url[1]}();
            } else {
                if (method_exists($controller, 'index')) {
                    $controller->index();
                }
            }
        }
    }
}
