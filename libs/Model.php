<?php

/**
 * @Author:    Florin Marina
 * @Date Created: 2018-05-02 09:28:09
 * @Last Modified by:    Florin Marina
 * @Last Modified time: 2018-05-02 09:29:32
 */
class Model
{
    public function __construct()
    {
        $this->db = new Database();
    }
}
