<?php

/**
 * @Author:    Florin Marina
 * @Date Created: 2018-05-02 09:03:23
 * @Last Modified by:    Florin Marina
 * @Last Modified time: 2018-05-02 13:52:14
 */
class Controller
{
    public $data = [];

    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * load model
     * @param $model
     */
    public function load_model($model)
    {
        $file = 'models/' . $model . '.php';

        if (file_exists($file)) {

            require 'models/' . $model . '.php';

            $this->{$model} = new $model();

        }
    }
}
