<?php

/**
 * @Author:    Florin Marina
 * @Date Created: 2018-05-02 09:10:16
 * @Last Modified by:    Florin Marina
 * @Last Modified time: 2018-05-02 12:23:40
 */
class View
{

    public function __construct()
    {
        //
    }

    /**
     * render the view
     */
    public function render($file_name)
    {
        require 'views/' . $file_name . '.php';
    }

    /**
     * my render for this app
     */
    public function load($content, $data = [])
    {
        $this->render('template/header_view');
        $this->render('template/navigation_view');
        $this->render('' . $content, $data);
        $this->render('template/footer_view');
    }
}
