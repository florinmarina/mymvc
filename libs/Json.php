<?php

/**
 * @Author: Florin Marina
 * @Date Created: 2018-05-02 13:50:00
 * @Last Modified by:   Florin Marina
 * @Last Modified time: 2018-05-02 14:42:47
 */

class Json
{

    /**
     */
    public function __construct()
    {
        //
    }

    /**
     * Output content
     *
     * @param mixed $content
     */
    public function output($content, $header_status_code = '')
    {
        $response = array(
            'status'  => true,
            'content' => $content,
        );

        header('Content-type: application/json');

        if ($header_status_code) {
            header('HTTP/1.0 ' . $header_status_code . ' OK');
        }

        echo json_encode($response);
        exit();
    }

    /**
     * Output error
     *
     * @param string $message
     */
    public function error($message, $header_status_code = '')
    {
        $response = array(
            'status'  => false,
            'content' => $message,
        );

        header('Content-type: application/json');

        if ($header_status_code) {
            header('HTTP/1.0 ' . $header_status_code . ' Error');
        }

        echo json_encode($response);
        exit();
    }

    /**
     * Is AJAX request?
     *
     * Test to see if a request contains the HTTP_X_REQUESTED_WITH header.
     *
     * @return  bool
     */
    public function is_ajax_request()
    {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }
}
