-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~xenial.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 02, 2018 at 06:13 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.29-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mymvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `product_number` varchar(20) CHARACTER SET utf8 NOT NULL,
  `product_desc` text CHARACTER SET utf8 NOT NULL,
  `product_image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `stock` decimal(11,4) NOT NULL,
  `buy_price` decimal(11,4) NOT NULL,
  `sell_price` decimal(11,4) NOT NULL,
  `vat_percent` smallint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_number`, `product_desc`, `product_image`, `stock`, `buy_price`, `sell_price`, `vat_percent`) VALUES
(25, 'Motorola Moto G4 Plus 16GB Smartphone', '123526254', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et rutrum dolor, eget tincidunt nulla. Donec ultricies venenatis quam eget hendrerit. Integer sed dictum est, ac fermentum ligula. Nullam sed vestibulum odio, vitae posuere lorem. Aenean sagittis accumsan quam, in semper libero euismod et. Sed eu turpis aliquet, tincidunt lacus non, feugiat ex. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.\r\n\r\nInteger vel eros porta, tincidunt elit a, semper leo. Nam ac eros ut urna dapibus imperdiet. Praesent in nulla ac odio mattis euismod ac in metus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla non eleifend odio. Mauris et tellus nibh. Proin ultrices sodales neque, in condimentum dolor malesuada vel. Vestibulum elementum mauris ante, eu mollis ipsum convallis eget. Nunc ullamcorper, leo luctus sodales maximus, risus ante tincidunt massa, at lacinia mauris tortor et ligula. Phasellus ac vehicula nibh, ut fermentum felis. Curabitur tempor neque vitae congue mollis. Morbi luctus egestas hendrerit. Vestibulum a convallis velit. Nam fermentum, ligula vitae elementum sodales, sem lorem accumsan mauris, eu accumsan lectus lacus ut nulla. ', 'FMAnsDFyAdOOmxYLV6mK2AYm8DRL6sNMfoaI1QgC35qpCpaxvx.jpg', '5.0000', '100.0000', '130.0000', 25),
(29, 'Apple iPhone 6s 16GB', '52362655544', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet, diam in consectetur scelerisque, dolor lorem laoreet tortor, quis fermentum mauris velit in tellus. Aenean et libero sit amet turpis dignissim tincidunt nec in turpis. Nunc ac dignissim dolor, vel dignissim orci. Proin in justo non orci faucibus euismod a sed nunc. Aenean iaculis sagittis convallis. Duis ligula sapien, sodales ac lectus sed, consequat ullamcorper turpis. Donec accumsan leo et mauris vulputate mattis. Aliquam finibus lectus id congue mollis.\r\n\r\nAenean sodales nulla enim, lacinia semper augue suscipit sit amet. Sed dignissim arcu et tincidunt vestibulum. Aliquam varius massa ut cursus dignissim. Quisque tincidunt vitae dui non varius. Ut ut magna massa. Vestibulum tristique a lacus eget sodales. Sed in fringilla lacus, auctor gravida elit. Praesent a ornare arcu. Vivamus volutpat efficitur tellus in egestas. Ut sodales, arcu in bibendum consequat, purus dolor ultricies ante, id ornare eros mauris at ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque tempor eget nulla sed lobortis. Etiam interdum suscipit eros, quis consectetur justo.\r\n\r\nProin iaculis rhoncus felis, rhoncus suscipit sapien convallis quis. Cras sagittis lectus vitae tortor ornare imperdiet. Donec sollicitudin lectus felis, non euismod tellus vestibulum in. Integer porttitor tempus urna, a gravida neque. Donec suscipit sit amet nisl ut auctor. Praesent in elit ac nunc aliquet suscipit. Pellentesque non fringilla arcu, vitae tempus est. Praesent cursus id lorem ut ultricies. Quisque lectus nisl, molestie eget porttitor vitae, dapibus eget metus. Suspendisse finibus non lorem eget bibendum. Nullam id felis dui. Integer vitae ante libero. Quisque ultricies mi metus, nec ultricies mauris pharetra quis. ', 'QfF4AsvaUrc7hALwXuteTMDe3JtXwuSnJyrk1WvVoH2GiOdfjG.jpg', '25.0000', '250.0000', '325.0000', 63);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
