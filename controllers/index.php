<?php

/**
 * @Author:    Florin Marina
 * @Date Created: 2018-05-02 09:40:12
 * @Last Modified by:    Florin Marina
 * @Last Modified time: 2018-05-02 10:07:19
 */

class Index extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->view->load('index/index');
    }

}

/* End of file index.php */
/* Location: .//home/clara/www/booking/controllers/index.php */
