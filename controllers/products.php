<?php

/**
 * @Author:    Florin Marina
 * @Date Created: 2018-05-02 04:40:12
 * @Last Modified by:    Florin Marina
 * @Last Modified time: 2018-05-02 17:56:49
 */

class Products extends Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load_model('Products_model');

        $this->json = new Json();

    }

    /**
     * index
     * @param $param1
     */
    public function index()
    {
        $this->view->load('products/all');

    }

    /**
     * View all products
     * @param
     */
    public function all($param = null)
    {

        $this->view->data['active_tab'] = 1;
        $this->view->data['products']   = $this->Products_model->get_all();

        $this->view->load('products/all', $this->data);
    }

    /**
     * Add Product controller
     */
    public function add()
    {
        $this->view->data['active_tab'] = 2;
        $this->view->load('products/add', $this->data);
    }

    /**
     * Add Product controller
     */
    public function edit($id)
    {
        $this->view->data['product'] = $this->Products_model->get_product($id);

        $this->view->load('products/edit', $this->data);

    }

    /**
     * add product ajax
     */
    public function add_product()
    {

        if ($this->json->is_ajax_request()) {

            if ($_FILES['image']) {

                $file = $_FILES['image'];

                if ($file['error'] == 0) {
                    $image = $_FILES['image']['name'];
                    $ext   = pathinfo($image, PATHINFO_EXTENSION);

                    $new_file = generate_random_string() . '.' . $ext;

                    $target_file = UPLOAD_PATH . $new_file;

                    move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

                } else {
                    $new_file = '';
                }
            }

            $data = [
                'product_name'   => $_POST['product_name'],
                'product_number' => $_POST['product_number'],
                'product_desc'   => $_POST['product_desc'],
                'stock'          => $_POST['stock'],
                'buy_price'      => $_POST['buy_price'],
                'sell_price'     => $_POST['sell_price'],
                'vat_percent'    => $_POST['vat_percent'],
                'product_image'  => $new_file,
            ];

            $this->Products_model->save_product($data);

            $this->json->output([
                'message' => 'Success',
                'url'     => base_url('products/all'),
            ]);
        }
    }

    /**
     * add product ajax
     */
    public function edit_product($id)
    {

        if ($this->json->is_ajax_request()) {

            if ($_FILES['image']) {

                $file = $_FILES['image'];

                if ($file['error'] == 0) {
                    $image = $_FILES['image']['name'];
                    $ext   = pathinfo($image, PATHINFO_EXTENSION);

                    $new_file = generate_random_string() . '.' . $ext;

                    $target_file = UPLOAD_PATH . $new_file;

                    move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

                } else {
                    $new_file = $_POST['product_image'];
                }
            }

            $data = [
                'product_name'   => $_POST['product_name'],
                'product_number' => $_POST['product_number'],
                'product_desc'   => $_POST['product_desc'],
                'stock'          => $_POST['stock'],
                'buy_price'      => $_POST['buy_price'],
                'sell_price'     => $_POST['sell_price'],
                'vat_percent'    => $_POST['vat_percent'],
                'product_image'  => $new_file,
            ];

            $this->Products_model->update_product($id, $data);

            $this->json->output([
                'message' => 'Success',
                'url'     => base_url('products/all'),
            ]);
        }
    }

    /**
     * delete product
     * @param $id
     */
    public function delete_product($id)
    {
        if ($this->json->is_ajax_request()) {

            $product = $this->Products_model->get_product($id);

            if (file_exists(UPLOAD_PATH . $product['product_image'])) {
                unlink(UPLOAD_PATH . $product['product_image']);
            }

            $this->Products_model->delete_product($id);

            $this->json->output([
                'message' => 'Success',
                'url'     => base_url('products/all'),
            ]);

        }
    }

}
