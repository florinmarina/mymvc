<?php

/**
 * @Author:    Florin Marina
 * @Date Created: 2018-05-02 05:10:54
 * @Last Modified by:    Florin Marina
 * @Last Modified time: 2018-05-02 09:07:55
 */
class Errors extends Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Description
     * @param $param1
     */
    public function controller()
    {
        echo 'Controller does not exists.';
    }
}
